## definition of colors

# normal
local DEFAULT="%{[m%}"
local BLACK="%{[0;30m%}"
local RED="%{[0;31m%}"
local GREEN="%{[0;32m%}"
local YELLOW="%{[0;33m%}"
local BLUE="%{[0;34m%}"
local MAGENTA="%{[0;35m%}"
local CYAN="%{[0;36m%}"
local LIGHT_GRAY="%{[0;37m%}"

#bold (grey is actually bold black)
local BGRAY="%{[01;30m%}"
local BRED="%{[01;31m%}"
local BGREEN="%{[01;32m%}"
local BYELLOW="%{[01;33m%}"
local BBLUE="%{[01;34m%}"
local BMAGENTA="%{[01;35m%}"
local BCYAN="%{[01;36m%}"
local BWHITE="%{[01;37m%}"

#underscore
local UBLACK="%{[04;30m%}"
local UGRAY="%{[01;04;30m%}"
local URED="%{[04;31m%}"
local UGREEN="%{[04;32m%}"
local UYELLOW="%{[04;33m%}"
local UBLUE="%{[04;34m%}"
local UMAGENTA="%{[04;35m%}"
local UCYAN="%{[04;36m%}"
local UWHITE="%{[04;37m%}"

#blinking 
local KGRAY="%{[01;05;30m%}"
local KRED="%{[05;31m%}"
local KGREEN="%{[05;32m%}"
local KYELLOW="%{[05;33m%}"
local KBLUE="%{[05;34m%}"
local KMAGENTA="%{[05;35m%}"
local KCYAN="%{[05;36m%}"
local KWHITE="%{[05;37m%}"

#ZSH_THEME="robbyrussell"


autoload -U compinit
compinit

autoload -U colors
colors

umask 002

zstyle ':completion:*' list-colors ''

# setopt multiosset

DIRSTACKSIZE=20

## please refer to "man zshoptions" for help

setopt auto_pushd           # auto directory pushd that you can get dirs by cd -[tab]
setopt pushd_to_home        # back to home with cd without any arguments
setopt list_packed          # compacked complete list display
setopt print_eight_bit      # show Japanese properly in completion lists
setopt no_clobber           # prevent overwrite ridirection
setopt no_unset             # prevent using undefined variables
setopt no_hup               # do not kill backgound jobs when logging out
setopt numeric_glob_sort    # regard number as numerical value and sort them in ascending order
setopt nolistbeep           # no beep sound when complete list displayed
setopt no_beep              # no beep sound when entering a wrong command
setopt ignore_eof           # do not log out by C-d
setopt pushd_ignore_dups    # remove older one of duplication in directory stack
setopt interactive_comments # allow to use comment while entering command
setopt complete_in_word     # try to complete a word by completion
setopt correct              # command correct edition before each completion attempt
setopt always_last_prompt   # show file list with cursor stayed in the current position
setopt auto_param_slash     # automatically add "/" when completing directory name
setopt mark_dirs            # add "/" when a directory is selected in file name list
setopt auto_menu            # automatically complete in order by typing completion key
setopt extended_glob        # completion by extended glob
setopt list_types           # show distinguishing mark in a completion list
setopt auto_cd

bindkey "Tab" menu-complete

## insert all expansions for expand completer
zstyle ":completion:*:expand:*" tag-order all-expansions

## formatting and messages
zstyle ":completion:*" verbose yes
zstyle ":completion:*:messages" format ${YELLOW}"%d"${DEFAULT}
zstyle ":completion:*:warnings" format ${RED}"No matches for:"${YELLOW}" %d"${DEFAULT}
zstyle ":completion:*:descriptions" format ${YELLOW}"completing %B%d%b "${DEFAULT}
zstyle ":completion:*:corrections" format ${YELLOW}"%B%d "${RED}"(errors %e)%b"${DEFAULT}
zstyle ":completion:*" group-name ""

zstyle ":completion:*:default" menu select=1
zstyle ":completion:*" completer _complete _approximate _prefix _match # _expand
zstyle ":completion:*:options" description "yes"

## match uppercase from lowercase
zstyle ":completion:*" matcher-list "m:{a-z}={A-Z}"

## offer indexes before parameters in subscripts
zstyle ":completion:*:*:-subscripts-:*" tag-order indexes parameters

## Environment variable configuration

export LANG=en_US.UTF-8

## Default shell configuration

## Set prompts
setopt prompt_subst
PROMPT='%{%(?.%{$fg_bold[green]%}.%{$fg_bold[red]%})%}%m%{$reset_color%}%# '
GROUP_PROMPT=`id | cut -d= -f4 | sed -r "s/([[:digit:]]+)\(([[:alnum:]]+)\)/%\1(g:\2:)/g" | sed "s/,//g"`
RPROMPT='%{$fg[white]%}[%{$fg[yellow]%}$GROUP_PROMPT%{%{$reset_color%}$fg[white]%}]%{$reset_color%} %{$fg_bold[blue]%}%~%{$reset_color%} %t'
SPROMPT='%{33m%} %BCurrent> '\''%r'\'' [Yes, No, Abort, Edit]%{[m%}%b '

## time
REPORTTIME=10
TIMEFMT="\
  The name of this job.               :%J
  CPU seconds spent in user mode.     :%U
  CPU seconds spent in kernel mode.   :%S
  Elapsed time in seconds.            :%E
  The CPU percentage.                 :%P
  The maximum memory usage            :%M Kb"
  

## set terminal title including current directory

case "${TERM}" in
kterm*|xterm)
  precmd() {
    echo -ne "\033]0;${USER}@${HOST%%.*}:${PWD}\007"
  }
  ;;
esac

## Command history

HISTFILE="${HOME}/.zhistory"
HISTSIZE=10000
SAVEHIST=10000
setopt extended_history      # manage starting time and spent time of a command
setopt hist_ignore_dups      # ignore duplication command history list
setopt hist_ignore_all_dups  # delete older one in case of already registered command
setopt share_history         # share command history data
setopt hist_reduce_blanks    # delete extra blanks

zstyle ":auto-fu:highlight" completion fg=black,bold

## Keybind configuration configuration

bindkey -e

## historical backward/forward search with linehead string binded to ^P/^N
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end  history-search-end
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end

setopt complete_aliases # aliased ls needs if file/dir completions work

## this line is for OpenFOAM bash to zsh
## if you use another version, change the directory
emulate sh -c 'source /opt/openfoam10/etc/bashrc'

alias la="ls -a"
alias ll="ls -l"
alias l="ls"
alias sl="ls"
alias ls="ls -F --color"
alias lss="ls -la"
alias s="ls -la"
alias paraview="/opt/paraviewopenfoam510/bin/paraview"
alias maintex='pdflatex main.tex ; pdflatex main.tex'
alias cleantex='rm -rf *.aux *.log'
alias gitadd='git add .; git commit -m "Automatic student commit"; git push'
alias showHidden='gsettings set org.gtk.Settings.FileChooser show-hidden true'
alias hideHidden='gsettings set org.gtk.Settings.FileChooser show-hidden false'
alias gcs='google-chrome-stable'


#export LSCOLORS=exfxcxdxbxegedabagacad
#export LS_COLORS='di=01;34:ln=36:so=32:pi=33:ex=32:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30:no=00:fi=00:or=01;05;37;41:*.tar=01;31:*.tar.gz=31:*.pdf=32:*.txt=35:*.eps=33:*.eps2=33:*.zip=31'
#zstyle ':completion:*' list-colors 'di=01;34' 'ln=01;36' 'so=32' 'ex=32' 'bd=46;34' 'cd=43;34' '*.tar=31' '*.tar.gz=31' '*.pdf=32' '*.txt=35' '*.eps=33' '*.eps2=33' '*.zip=31'

path=($path /opt/soscfd/lib)
path=($path /opt/soscfd/LIB)

#export PYTHONPATH=/usr/bin/python3:$PYTHONPATH
#export DISPLAY=localhost:0.0

